
Untitled Manuscript
===================

Team^^

# About Manuscripts {#MPSection:EE27D1FE-4CD0-455B-AA83-407292CFE49C}

Manuscripts is designed for a seamless writing process from start to finish of a large writing project. We want to especially serve writing large scholarly documents: literature reviews, Master's dissertations, PhD theses, journal articles, book chapters, grant applications, patent applications, the list goes on.

# What makes Manuscripts special? {#MPSection:80103933-ACB5-403B-8BF4-571AB4A03BF3}

Manuscripts helps a writer to focus on producing great work throughout a writing project, from planning to publishing stage.

-   You can start writing using a **manuscript template**, by importing an existing document, or by creating a new empty manuscript.
-   You can **outline and plan** your writing project using a powerful manuscript outline that updates live as you write.
-   Editing has been tuned to needs of scholarly writing: **clean, simple and built for purpose**.
-   **Top of the range citing workflow** and automated bibliography formatting either using Magic Citations from Papers, or a built-in citation tool that imports references from pretty much any common format: EndNote XML, RIS, BibTeX, Citeproc JSON, MODS, and several more.
-   Cross-referencable **multi-panel figures, tables and equations**.
-   **Import and export** key file formats used for scholarly writing: MS Word, LaTeX, or Markdown.
-   The manuscript file format is **versioned**.
-   For those writing peer reviewed literature, Manuscripts will allow you to **directly submit your work to thousands of different journals**.

# How to get started? {#MPSection:6C9A4D00-C865-4C74-EB85-E2C2ECE92D7B}

There are three ways to get started with Manuscripts.

1.  Select one of our **built-in templates**. Manuscripts comes with over 1,100 verified templates, as well as unverified templates based on CSL styles. We are building literally thousands of them for many types of documents, including for journals across all sciences, university specific PhD theses, grant applications etc.
2.  **Import** an existing document. Manuscript supports multiple import formats such as MS Word, LaTeX, Markdown and many more.
3.  Create a new blank manuscript.

## Selecting a built-in template {#MPSection:21C910B9-C417-49EF-E52D-9F7B3A04FC83}

Choose ‘Create new manuscript’ from the welcome screen or if do not have the welcome screen available, go to File &gt; New &gt; Manuscript with Template…

<div id="MPFigureElement:4A4020D2-7127-4989-F899-6303B871B039">

![](Figure_1.png)

Figure 1: Template selector

</div>

Browse or search for available templates. Manuscript-verified templates are indicated by a badge icon as shown in Figure 1. Once you have found and selected the template you need, click Choose and a manuscript template is opened for you.

<div id="MPFigureElement:EE985B72-899D-4B7A-E819-62722915E269">

![](Figure_2.png)

Figure 2: New manuscript from template

</div>

### I cannot see a template for the document I would like to write, what should I do? {#MPSection:57A4E893-15FB-4423-AD09-302CD85D2ED1}

In case you cannot find the template you are looking for, do not worry, you still have several options to get started.

If you are looking to get started with a generic template that consists of an abstract, introduction, methods, results and discussion, you can choose a different journal template (e.g. Genome Biology Research Article) and modify the template to your needs. Remember that the template is only there to help you get started and to provide you with guidance. However, you are still the master of your own manuscript.

If you rather get started with a blank canvas, choose **Create Empty Manuscript** option from the bottom of the template selector and you are all set to write your first document with Manuscripts.

You can contact us at support@manuscriptsapp.com with a link to the journal/template style you would like Manuscripts to support and we will do our best to add your requested template to Manuscripts.

### I can find a template but it does not have the ‘Manuscript verified’ badge next to it. {#MPSection:BA373CDF-A773-4E33-A67B-582128999D28}

We have added over 7,000 journal CSL styles that you can choose to get started from to create a template of your own. If you find a journal template without a badge next to the journal name it means that we have not verified the template for that journal yet.

You can still choose the journal template as your starting point. When you select the template a pop up window appears where you can see and edit basic template metadata such as which sections it should contain, which font and font sizes will be used, and so on. Once you are ready, click **Create Manuscript**.

## Import an existing document to Manuscripts {#MPSection:22D273EE-9162-49EF-94BF-2AD1B9D17861}

If you already have a draft in another document format and you would like to continue writing it with Manuscripts, you can easily import the document to Manuscripts and continue writing your draft with ease.

1.  Create a new empty manuscript from **File &gt; New &gt; Manuscript.**
2.  Go to **File &gt; Import &gt; Content**..
3.  You can see all the supported document formats that can be imported to Manuscripts by clicking ‘Options’ in the file browser window.
4.  Choose the document that you want to import to Manuscripts, click Open and the document is opened as a new manuscripts file.

<div id="MPFigureElement:E7D0EEB3-25FB-440C-C084-C4D12EC17FE0">

![](Figure_3.png)

Figure 3: Import an existing document to Manuscripts

</div>

# How to reorder manuscript sections {#MPSection:935A6DB6-1D8C-4EB0-874C-749431DF5E78}

The manuscript outline lets you not just see and navigate an outline of your manuscript project, but also manipulate the structure of it in powerful ways. Simply drag and drop to reorder sections or paragraphs to change the flow of your document.

# How to use the focus mode in Manuscripts {#MPSection:53C448BB-E747-4EF6-CDB4-9E08B0298188}

Maintaining focus is essential to productive writing. One of the ways we help you focus in your work is the so-called **focus mode**. You can activate the focus mode in two ways.

A. Click **View &gt; Selected Sections** in the main menu.

B. Click the **focus button** in the manuscript outline, visible when you hover on an individual section in the outline on its right edge.

To toggle back to viewing the entire manuscript, simply

A. Click **View &gt; View All Sections** in the main menu

or

B. Click the focus button again

<div id="MPFigureElement:65CF04E4-DF53-46FA-A181-E37AAEE2A65F">

![A. Focus mode in action.](Figure_4_1.png)

![B. Focus mode icon](Figure_4_2.png)

Figure 4: 1. The manuscript editing window when you have activated the focus mode. 2. The focus mode icon, visible when you hover on a section item.

</div>

# How to use the gutter menus {#MPSection:90AF73D4-0C13-4BA2-C0B5-61322E3D133D}

The Manuscripts editor includes so-called "gutter helpers", designated with the blue **+** and **o** symbols in the left margin for the currently active paragraph. These symbols are buttons that lead you to shortcut actions for inserting (the**+**symbols) and manipulating (the **o**) the document contents in a way that is contextual.

## The gutter insertion helper (+ symbol) {#MPSection:5FFD20E9-09A5-4288-B5AD-5D76F90D2A4A}

<div id="MPFigureElement:45357EED-624B-4B39-DE3E-0EF07DDEC797">

![](Figure_5.png)

Figure 5: How to create a new section

</div>

## The gutter selection helper (o symbol) {#MPSection:8C432E2D-446D-4D05-D33A-FD2E79449A97}

The following example shows what the gutter selection helper tool presents in the case of a paragraph.

<div id="MPFigureElement:DAAE3273-512B-45B9-C399-8549458045DC">

![](Figure_6.png)

Figure 6: The gutter selection helper for a paragraph.

</div>

The gutter helper is entirely optional and can be toggled on or off from the Editing Preferences by choosing "Show smart gutter in the left margin when editing”.

<div id="MPFigureElement:5E155CDD-76FA-4201-9691-3C19BCBB02C6">

![](Figure_7.png)

Figure 7: Smart gutter can be enabled or disabled in the Editing Preferences

</div>

# Exporting a selected subset of a manuscript {#MPSection:506CF71B-2E88-4C3E-E78B-46D28809CE2F}

It often comes handy to place material in your manuscript project which is never intended to reach readers. For instance you may want to keep…

1.  **Notes** regarding your research or writing progress: material that helps you organise your writing, but is never intended for publication or for feedback.
2.  **Unfinished material** that is not yet ready for feedback or publication.

Manuscripts supports this way of writing by allowing you to choose what is shared or exported by first making an **outline selection** and then sharing or exporting.

## Share the current selection {#MPSection:E3201021-72F9-447A-E10D-75662EDBBF1F}

-   Select what you want to share in the manuscript outline (⇧+click or ⌘+click to select multiple sections).
-   Choose the applicable option from **Publish &gt; Share &gt;….** The export formats made available for sharing are:
    -   the manuscript file format (.manu, which is a compressed variant of the file format).
    -   MS Word (docx)
    -   Your default markup format (which you can choose from **Preferences &gt; Editing**): one of Markdown, LaTeX or HTML at the time of writing.
    -   PDF

<div id="MPFigureElement:4826B479-0949-43F5-99EF-A56647AE0A3A">

![](Figure_8.png)

Figure 8: Sharing the current selection

</div>

Note that if you have selected your bibliography as one of the items you want to export, only the subset of it that is cited in the selected part of your manuscript is included in the bibliography.

## Export the current selection {#MPSection:2394B16D-52F9-42F3-8A42-8C7B4D8D3449}

-   Select what you want to export in the manuscript outline (⇧+click or ⌘+click to select multiple sections).
-   Click **File &gt; Export** (⌥⌘K)
-   Choose **Export selected content only**.

As with sharing, if you have selected your bibliography as one of the items to export, only the subset of citations in the selected part of your manuscript get included in the bibliography.

# Spell and grammar checking in Manuscripts {#MPSection:6E378C0A-0AE3-4CF0-E45C-84690C33A29E}

Manuscripts allows you to check your spelling and grammar as you type to avoid disasters like the one shown in Figure 10. Grammar and spellchecking can be accessed from **Edit &gt; Spelling and Grammar** in the main menu.

<div id="MPFigureElement:F1D26FC9-DE58-4CBA-F54E-826F1C35465F">

![](Figure_9.png)

Figure 9: Enable spell and grammar check

</div>

## Check spelling and grammar manually {#MPSection:15E00D03-5467-402D-C77A-9C8482F89143}

To check spelling and grammar manually, choose **Edit &gt;Spelling and Grammar &gt; Show Spelling and Grammar (⌘:).**

<div id="MPFigureElement:BF42A018-8F23-4A53-C61C-D556C6D1B4A5">

![](Figure_10.png)

Figure 10: Check spelling and grammar manually

</div>

To update the spelling and grammar analysis of your document after you have made some changes, choose **Edit &gt;Spelling and Grammar &gt; Check Document Now** **(⌘;)**.

## Check spelling and grammar as you type {#MPSection:91DCED50-541D-4C7C-F6A2-E49E318600B0}

Spelling and grammar can also be checked as you type, by toggling on **Edit &gt; Spelling and Grammar &gt; Check Spelling While Typing**. Grammatical errors are presented in the familiar green, and spelling errors in red.

<div id="MPFigureElement:FD4019EF-FA69-47CE-83C4-55884BA8CEE0">

![](Figure_11.png)

Figure 11: Check spelling and grammar while typing

</div>

## Correct spelling automatically {#MPSection:23D22A13-379B-452B-FAFB-216BAD53C8C3}

You can even make Manuscripts correct your spelling for you by choosing **Edit &gt; Spelling and Grammar &gt; Correct Spelling Automatically**.

# Choosing a citation style {#MPSection:022D832B-4DD0-4FCF-F860-7694F573CE40}

You can change the manuscript's citation style in the inspector available on the right hand side of the main window by clicking the **Toggle Inspector** button available in the lower right corner of the application window.

<div id="MPFigureElement:130155FB-C641-45AC-8A28-CF9704A4B11F">

![](Figure_12.png)

Figure 12: Inspector toggle button icon

</div>

This reveals the inspector which includes two tabs, second of which is the style inspector. At the bottom it includes the **Manuscript** **Styles inspector** palette (where you see the keyboard focus in the screenshot below). This inspector palette is where you can change the citation style.

<div id="MPFigureElement:7BC29E55-FF73-457C-B06D-26F1A0CEDF87">

![](Figure_13.png)

Figure 13: Changing the citation style in the Inspector

</div>

# How to change the font family and size? {#MPSection:2E7FF19C-F513-4746-E534-BF41445257D3}

You can change the font family and size and edit other properties of paragraph styles in the inspector on the right hand side of the main window by clicking the **Toggle Inspector** button (available in the lower right corner of the application window).

<div id="MPFigureElement:2D990E69-332E-4E89-9561-84C4D25AE330">

![](Figure_14.png)

Figure 14: Inspector toggle button icon

</div>

This reveals the inspector which has two tabs, second of which is the style inspector that includes the **Paragraph Styles palette** that let you choose font sizes, etc.

<div id="MPFigureElement:564E88C4-FC29-421B-E807-9714980B0A0B">

![](Figure_15.png)

Figure 15: Change font and font size in the Paragraph Styles section of the Inspector

</div>

# How to create a figure panel {#MPSection:C64373E8-230A-40F7-8E0D-0595EBF4A9ED}

The basic workflow for adding figure panels into your Manuscripts document involves first placing a figure placeholder into your document (which you can caption at this stage), and then adding an image as a second step.

Importing manuscript content that includes images is another option, i.e. **File &gt; Import &gt; Content.** This is not further discussed separately here.

## Creating an empty figure panel {#MPSection:601DB2F2-B96A-4CA4-DEBC-6D5CCE4727CD}

There are three different ways to create an empty figure. Each one of them requires you to first place the text insertion cursor next to the spot where you want to insert the figure. After that, you can either:

-   Click the figure symbol in the toolbar.
-   Use the menu item Insert &gt; Figure in the main menu.
-   Use the smart gutter menu to the left of the paragraph that is in focus, before or after which you intend to add the figure. Click the blue + symbol in it, and choose “Insert Figure”.

<div id="MPFigureElement:25D858F6-4677-4D28-8F5E-5F59DB8F93C1">

![A. Figure panel icon](Figure_16_1.png)

![B. ](Figure_16_2.png)

Figure 16: How to create a figure panel

</div>

The figure placeholder you create will look something like the following:

<div id="MPFigureElement:F1307A83-8179-4447-B0E7-654A7B9D8592">

![](Figure_17.png)

Figure 17: New empty figure panel

</div>

## Adding images to a figure panel {#MPSection:7D4AAB5B-3CE8-4F0D-86E3-5605DA771C2B}

You can add images to a figure panel in three ways, starting from an empty placeholder image.

1.  By dragging and dropping into the figure + symbol.
2.  By clicking the figure + symbol and choosing **Choose File to Import…**
3.  By clicking the figure + symbol and choosing amongst orphaned figures in the manuscript in case you have previously added and then removed figures (orphaned figure images can be removed permanently by right clicking the figure in the list of orphaned figures, and choosing **Delete**).

To replace a figure, simply either:

1.  Drag an image on top of the image you wish to replace.
2.  Alternatively, click on the image and drag in the popover that opens, or again **Choose File to Import…**

## Creating a multi-panel figure {#MPSection:D7758474-86A8-4C57-E4D3-BD9AE5E62E45}

You can create a multi-panel figure in Manuscripts simply by:

<div id="MPFigureElement:E72EFA55-BD6F-46BF-FD36-5996D9AA2D55">

![](Figure_18.png)

Figure 18: Creating a multi-panel figure

</div>

## Figure file formats {#MPSection:DB7EF5AF-D680-46D6-DD5C-A536F61BC434}

Manuscripts always deals with image data in a non-destructive way. For instance figure panel images all internally still store the original full sized image even if any panel members need to be scaled. Similarly, Manuscripts keep vector formatted graphics in their original form and only rasterises them to bitmap images on export if required (see below).

Some manuscript export formats have specific restrictions on the format in which you need to store your images. You should not have to worry about any aspect of image data other than where the images reside on your disk. Manuscripts handles the rest for you transparently: the export format, and manuscript templates even define rules for formatting and exporting of figures that are automatically followed on export.

# How to create a table {#MPSection:355EA528-012A-4E41-FE34-37F5A2D0E029}

Manuscripts allows the users to create tables that are easy to fill in, and simple to edit by adding or removing columns and rows.

There are three different ways to create an empty table. Each one of them requires you to first place the text insertion cursor next to the spot where you want to insert the table. After that, you can either:

-   Click the table figure in the toolbar.
-   Use the menu item **Insert &gt; Table**.
-   User the smart gutter menu to the left of the paragraph that is in focus, before or after which you intend to add the table. Click on the blue + symbol in it, and choose **Insert Table**.

<div id="MPFigureElement:42F7C896-66B5-4A40-FE48-0D700EEEF320">

![A. Table icon](Figure_19_1.png)

![B. ](Figure_19_2.png)

Figure 19: How to create a table in Manuscripts

</div>

The table that is created looks like this:

<div id="MPFigureElement:FD2A5EFA-F72F-452D-DA2D-CF09015A4987">

![](Figure_20.png)

Figure 20: New table

</div>

To add or remove rows or columns, right-click any of the columns or rows and choose the relevant option from the context menu.

<div id="MPFigureElement:D0EA053A-DEAE-4788-90EF-875DF0FC0EE7">

![](Figure_21.png)

Figure 21: Editing a table in Manuscripts

</div>

## How to edit the table format in Manuscripts {#MPSection:8B7656AA-8D98-4FB8-FC6D-07A02AB90A1C}

You can change and edit the table format with the table styles inspector available on the right hand side of the main window by clicking the **Toggle Inspector** button (located in the lower right corner of the application window):

<div id="MPFigureElement:8CBEB9B7-8796-44FC-BCC4-6FE3B6D0624F">

![A. Inspector Toggle icon](Figure_22_1.png)

![B. Table Styles in the Inspector](Figure_22_2.png)

Figure 22: Edit and change table styles in the Inspector

</div>

# How to create an equation {#MPSection:829CFDAA-D5E7-429E-EC64-319DB27E9CF5}

Manuscript includes a powerful equation editor that allows you to insert beautiful equations using LaTeX.

## Creating an empty equation {#MPSection:2C203616-B4D8-48EB-8E68-A1C0607278E2}

There are three different ways to create an empty figure. Each one of them requires you to first place the text insertion cursor next to the spot where you want to insert the figure. After that, you can either:

-   Click the equation symbol in the toolbar.
-   Use the menu item **Insert &gt; Equation** in the main menu.
-   Use the smart gutter menu to the left of the paragraph that is in focus, before or after which you intend to add the figure. Click on the blue + symbol in it, and choose **Insert Equation**.

<div id="MPFigureElement:506F236B-D166-4CEE-D8A3-B7F3FA2181CE">

![A. Equation icon](Figure_23_1.png)

![B. ](Figure_23_2.png)

Figure 23: Creating an equation

</div>

The equation placeholder that is inserted to the manuscript looks like this:

<div id="MPFigureElement:4D66A47E-99CF-4AB9-F5F7-3F30C8D58F83">

![](Figure_24.png)

Figure 24: Equation placeholder

</div>

## How to edit an equation {#MPSection:99FA93F2-EF6E-4339-BC94-E59F33D327F3}

To edit an equation, click on the equation so that the equation editor pops up. Now you can type or paste your equation using LaTeX, the equation will be rendered in real time. If you are not familiar yet with the LaTeX syntax for equations you can find out more here: https://en.wikibooks.org/wiki/LaTeX/Mathematics\#Symbols

<div id="MPFigureElement:C3180A49-5260-4963-8599-7525A906ECD8">

![A. ](Figure_25_1.png)

![B. ](Figure_25_2.png)

Figure 25: Editing an equation in Manuscripts

</div>

# Manuscript file versioning: backups & change tracking {#MPSection:754628D5-DF0E-4681-D783-3AD482544BC9}

In short, your Manuscripts documents are versioned for backup and rollback purposes right now. It will also form the basis for change tracking features we intend to build after 1.0.

The Manuscripts file format is fully version controlled and provides an "infinite" history (all the way back to its beginning), at the level of each edit you make.

-   All edits you make to the manuscript go into an infinite version history already.
-   There is no user interface in the app yet to present this embedded version history.
-   We intend to build some exciting features based on this version history after the 1.0 version: **change tracking** and **snapshotting** named versions of your document.

Manuscripts also stores versions of the document that you edit on each device in a separate, redundant backup and rollback intended version history.

-   You can view this version history with **File &gt; Versions &gt; Browse Earlier Versions.**
-   With this file versioning feature you can roll back to earlier revision stored on the device where you made that version.
-   This version history is a redundant backup mechanism for your data intended to protect your data and allow going further back in time than what the undo history of each app session allows.

However, you can never be too careful and we still recommend you to keep a regular Time machine backup of your work.

# Citing with and without the Papers reference manager {#MPSection:6B37A5AB-234E-43CD-FE5A-31A708A6C1DF}

Our recommended reference manager to use together with Manuscripts is <span class="citation" data-href="http://papersapp.com/">Papers</span>. Papers includes a citation tool <span class="citation" data-href="http://support.mekentosj.com/kb/cite-write-your-manuscripts-and-essays-with-citations/magic-citations-on-papers-3-for-mac">Magic Citations</span>, which works beautifully with our app.

However, Papers is just one option available to you amongst other reference management tools like Zotero, Mendeley, EndNote, etc. You can use any of these tools together with Manuscripts because it includes a super easy to use citation tool that allows you to cite papers without directly requiring to interact with an external reference manager. This is what the Manuscripts citation tool looks like:

<div id="MPFigureElement:E343D690-81E2-4FB9-A6B0-53651A08BF97">

![](Figure_26.png)

Figure 26: Using the citation tool within Manuscripts

</div>

## Importing bibliography data into Manuscripts {#MPSection:4A11B9CF-01CB-4184-F2B8-C20876467962}

We support importing references from all the major reference file formats (Endnote XML, RIS, BibTeX and more). All popular reference managers can export to at least one of these formats. To insert references, either:

1.  Choose<span id="bdb7b322-6c7d-4844-f019-fc20082e49cb" class="manuscript-bold"> **File &gt; Open** in the main menu to open a bibliography file that you exported from your favorite reference manager.</span>
2.  Choose **File &gt; Import** in the main menu to import bibliography data into your currently open manuscript.
3.  Drag a bibliography file into the Manuscripts dock icon.

## Configuring a citation keyboard shortcut {#MPSection:A6971BA0-6988-47A9-EFBB-9C9AE358A89E}

You can configure the keyboard shortcut to use for the internal citation tool with the option available at **Preferences &gt; Editing &gt; Citation Shortcut**:

<div id="MPFigureElement:BF2F041F-96AF-483E-D836-AA044F8C0CF0">

![](Figure_27.png)

Figure 27: Configuring a citation tool shortcut in Editing Preferences

</div>

# Enjoy writing your next best work with Manuscripts {#MPSection:E20B2DCB-6AFA-4900-E0C6-7116BAA9CC38}

We hope you really enjoy writing with Manuscripts. Visit our forums for more information and up-to-date answers to questions you may have: http://support.manuscriptsapp.com. Please contact us via support@manuscriptsapp.com if you would like to talk to us or hit issues with the app. We would love to hear what you think of Manuscripts and how we can make it even better. Enjoy!

*The Manuscripts team*
